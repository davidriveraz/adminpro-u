import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-incrementator',
  templateUrl: './incrementator.component.html',
  styles: [
  ]
})
export class IncrementatorComponent implements OnInit {
  @ViewChild('txtProgress') txtProgress: ElementRef;
  @Input() leyenda: string = 'Leyenda';
  @Input() progreso = 50;


  @Output() cambioValor: EventEmitter<number> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {

  }
  onChanges(newValue: number) {
    console.log(this.txtProgress);

    if (newValue >= 100) {
      this.progreso = 100;
    } else if (newValue <= 0) {
      this.progreso = 0;
    }
    this.txtProgress.nativeElement.value = this.progreso;
    this.cambioValor.emit(this.progreso);
    this.txtProgress.nativeElement.focus();
    
  }


  cambiarValor(i: number) {
    if (this.progreso >= 100 && i > 0) {
      this.progreso = 100;
      return;
    }
    if (this.progreso <= 0 && i < 0) {
      this.progreso = 0;
      return;
    }
    this.progreso = this.progreso + i;
    this.cambioValor.emit(this.progreso);
  }
}
