import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../services/usuario/usuario.service';
import { format } from 'path';
import { Usuario } from '../models/usuario.model';

declare function ini_plugins();
declare const gapi: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  recuerdame: boolean = true;
  email: string;
  auth2: any;
  constructor(public router: Router, public _usuario: UsuarioService) { }
  
  ngOnInit(): void {
    this.email = localStorage.getItem('email') || '';
    if(this._usuario.estaLogueado()){
      this.router.navigate(['/dashboard']);
    }
    ini_plugins();
    this.googleInit();
  }
  googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '627373365557-bdf5khklc7l44j1j28pjqlnifq7793dv.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile'
      })
      this.attachSignin( document.getElementById('btnGoogle') );
    })
   
  }

  attachSignin(element) {
    this.auth2.attachClickHandler( element, {}, (googleUser) => {

      let profile = googleUser.getBasicProfile();
      let token = googleUser.getAuthResponse().id_token;
      
      this._usuario.loginGoogle( token )
              .subscribe( (res) => { 
                console.log(res);
                
                window.location.href = '#/dashboard'  });

    });
  }
  ingresar(f: NgForm) {
    if (f.invalid) {
      return;
    }
    let usuario = new Usuario(
      null, f.value.email, f.value.password
    )
    this._usuario.login(usuario, f.value.recuerdame).subscribe(
      res => this.router.navigate(['/dashboard'])
    )

  }
}
