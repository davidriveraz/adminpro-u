import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { SettingsService } from '../../services/settings/settings.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: [
  ]
})
export class AccountSettingsComponent implements OnInit {

  constructor(@Inject(DOCUMENT) private _document, public _ajustes: SettingsService) { }

  ngOnInit(): void {
    this.colocarCheck();
  }
  cambiarColor(tema: string, link: any) {
    console.log(link);

    let url = `assets/css/colors/${tema}.css`;
    this._document.getElementById('tema').setAttribute('href', url);
    this._ajustes.ajustes.tema = tema;
    this._ajustes.ajustes.temaUrl = url;
    this.aplicarCheck(link);
    this._ajustes.guardarAjustes();

  }

  aplicarCheck(link: any) {
    let selectores: any = document.getElementsByClassName('selector');
    for (let sel of selectores) {
      sel.classList.remove('working');
    }
    link.classList.add('working');
  }
  colocarCheck() {
    let selectores: any = document.getElementsByClassName('selector');
    for (let sel of selectores) {
      sel.classList.remove('working');
    }
    let tema = this._ajustes.ajustes.tema;
    for (let sel of selectores) {
      if (sel.getAttribute('data-theme') === tema) {
        sel.classList.add('working');
      }
    }
  }
}
