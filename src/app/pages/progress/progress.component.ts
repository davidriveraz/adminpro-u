import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styles: [
  ]
})
export class ProgressComponent implements OnInit {
  progreso:number=75;
  progreso2=20;
  leyenda='Primera leyenda';
  leyenda2="segunda leyenda";
  constructor() { }

  ngOnInit(): void {
  }

  actualizar(event:number){ 
    this.progreso=event;
    
  }

}
