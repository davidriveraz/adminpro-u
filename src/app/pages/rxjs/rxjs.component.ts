import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, pipe, observable, Subscription } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators'
@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnInit,OnDestroy {
  subscription:Subscription;
  constructor() {

   this.subscription=this.regresaObservable().pipe(
      retry(2)
    ).subscribe(
      numero => console.log('Subs', numero),
      error => console.log('Error en el observer ', error),
      () => console.log('El observador termino !')
    )
  }

  ngOnInit(): void {
  }
  ngOnDestroy():void{
  this.subscription.unsubscribe();
    
  }
  regresaObservable(): Observable<any>{
    return new Observable(observer => {
      let contador = 1;
      let intervalo = setInterval(() => {
        contador++;
        const salida= {
          valor:contador
        };

        observer.next(salida);
        /* if (contador == 3) {
          clearInterval(intervalo);
          observer.complete();
        } */
        /* if (contador == 2) {
          clearInterval(intervalo);
          observer.error('Auxilio!!');
        } */
      }, 1000);


    })

    
  }
}