import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {
  constructor(public _usuario:UsuarioService,public _router:Router){

  }
  canActivate() {
    if(this._usuario.estaLogueado()){
      
      return true;
    }
    else{
      this._router.navigate(['/login'])
      return false;
    }
   
  }

}
