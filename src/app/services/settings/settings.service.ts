import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  ajustes: Ajustes = {
    temaUrl: 'assets/css/colors/green-dark.css',
    tema: 'green-dark'
  }
  constructor(@Inject(DOCUMENT) private _document) {
    this.cargarAjustes();
   }
  guardarAjustes() {
    localStorage.setItem('ajustes', JSON.stringify(this.ajustes));
  }
  cargarAjustes() {
    if (localStorage.getItem('ajustes')) {
      this.ajustes = JSON.parse(localStorage.getItem('ajustes'));
    }else{

    }
    this.aplicarTema();
  }
  aplicarTema(){
    let url = this.ajustes.temaUrl;
    this._document.getElementById('tema').setAttribute('href', url);
  }
}


interface Ajustes {
  temaUrl: string;
  tema: string;
}