import { Injectable } from '@angular/core';
import { Usuario } from '../../models/usuario.model';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from 'src/app/config/config';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  usuario: Usuario;
  token: string;
  constructor(public http: HttpClient,public _router:Router) {
    console.log('Servicios de usuario implementado');
    this.cargarStorage();
  }
  estaLogueado(){
    return (this.token.length>5) ?true:false;
  }
  cargarStorage(){
    if(localStorage.getItem('token')){
      this.token=localStorage.getItem('token');
      this.usuario=JSON.parse(localStorage.getItem('usuario'));      
    }
    else{
      this.token='';
      this.usuario=null;
    }
  }
  login(usuario: Usuario, recordar: boolean = false) {
    if (recordar) {
      localStorage.setItem('email', usuario.email);
    }
    else {
      localStorage.removeItem('email');

    }
    let url = URL_SERVICIOS + '/login';
    return this.http.post(url, usuario).pipe(map((res: any) => {
      this.guardarStorage(res.usuarioDB._id, res.token, res.usuarioDB);
      return res;
    }))
  }
  loginGoogle(token: string) {

    let url = URL_SERVICIOS + '/login/google';

    return this.http.post(url, { token }).pipe(map((resp: any) => {
      this.guardarStorage(resp.usuarioDB._id, resp.token, resp.usuarioDB);
      return resp;
    })
    )



  }
  logout(){
    this.usuario=null;
    this.token='';
    localStorage.removeItem('usuario');
    localStorage.removeItem('token');
    this._router.navigate(['/login']);
  }
  guardarStorage(id: string, token: string, usuario: Usuario) {

    localStorage.setItem('id', id);
    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));

    this.usuario = usuario;
    this.token = token;
  }
  crearUsuario(usuario: Usuario) {
    let url = URL_SERVICIOS + '/usuarios';
    return this.http.post(url, usuario).pipe(map(res => {
      swal('Genia', "EL usuario se ha creado", 'success');
      return res;
    }))
  }
}
