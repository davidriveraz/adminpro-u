import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario/usuario.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent implements OnInit {
  nombre="";
  correo="";
  usuario:any;
  constructor(public _usuario:UsuarioService) { }

  ngOnInit(): void {
    this.usuario=JSON.parse(localStorage.getItem('usuario'));
    this.nombre=this.usuario.nombre;
    this.correo=this.usuario.email;
  }

}
