
import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


@NgModule({
  imports:[
    RouterModule,
    CommonModule
  ],
  declarations: [
    SidebarComponent,
    HeaderComponent,
    NopagefoundComponent,
    BreadcrumbsComponent,
  ],
  exports:[
    SidebarComponent,
    HeaderComponent,
    NopagefoundComponent,
    BreadcrumbsComponent,
  ],
  
})
export class SharedModule { }
