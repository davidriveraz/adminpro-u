import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/service.index';
import { UsuarioService } from '../../services/usuario/usuario.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit {
  nombre=""
  constructor(public _sidebar:SidebarService,public _usuario:UsuarioService) { }

  ngOnInit(): void {
    this.nombre=JSON.parse(localStorage.getItem('usuario')).nombre;
  }

}
